package game;

import java.util.Random;
import java.util.Scanner;

public class Player {
        private String name;
        private int health;
        private int damage;

    public Player(Scanner scanner) {
        System.out.print("Masukan Nama Player: ");
        this.name = scanner.nextLine();
        System.out.print("Masukan Darah Player: ");
        this.health = scanner.nextInt();
        System.out.print("Masukan Damage Player: ");
        this.damage = scanner.nextInt();
        System.out.println("=====================================");
    }

        public String getName() {
            return name;
        }

        public int getHealth() {
            return health;
        }

        public void takeDamage(int damage) {
            health -= damage;
        }

        public int attack() {
           Random random = new Random();
              int randomDamage = random.nextInt(damage);
              return randomDamage;
        }




}
