package game;

import java.util.Random;
import java.util.Scanner;

public class Enemy {
    private String name;
    private int health;
    private int damage;

    public Enemy(Scanner scanner) {
        System.out.print("Masukan Nama Musuh: ");
        this.name = scanner.nextLine();
        System.out.print("Masukan Darah Musuh: ");
        this.health = scanner.nextInt();
        System.out.print("Masukan Damage Musuh: ");
        this.damage = scanner.nextInt();
    }

    public String getName() {
        return name;
    }

    public int getHealth() {
        return health;
    }

    public void takeDamage(int damage) {
        health -= damage;
    }

    public int attack() {
        Random random = new Random();
        int randomDamage = random.nextInt(damage);
        return randomDamage;
    }
}
