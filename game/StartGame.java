package game;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class StartGame {
    public static void main(String[] args) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
        Date date = new Date();
        Scanner scanner = new Scanner(System.in);
        Scanner scanner2 = new Scanner(System.in);

        System.out.println("Selamat Datang di game JAVA!");
        System.out.println("Tanggal: " + sdf.format(date));
        System.out.println("====================================="  );
        Player player = new Player(scanner);
        Enemy enemy = new Enemy(scanner2);

        System.out.println("==============================");

        System.out.println("Kamu akan melawan -> " + enemy.getName());
        System.out.println("Darah Player : " + player.getHealth());
        System.out.println("Darah Musuh : " + enemy.getHealth());
        System.out.println("-------------------------------------");

        // Fight!
        while (player.getHealth() > 0 && enemy.getHealth() > 0) {
            int playerAttackDamage = player.attack();
            int enemyAttackDamage = enemy.attack();
            System.out.println("Pemain menyerang musuh dengan " + playerAttackDamage + " damage.");
            System.out.println("Darah Musuh : " + enemy.getHealth());
            enemy.takeDamage(playerAttackDamage);
            if (enemy.getHealth() <= 0) {
                break;
            }
            System.out.println("Musuh menyerang Player dengan " + enemyAttackDamage + " damage.");
            System.out.println("Darah Player : " + player.getHealth());
            player.takeDamage(enemyAttackDamage);
            System.out.println("-------------------------------------");
        }

        // Determine the winner
        if (player.getHealth() > 0 && enemy.getHealth() <= 0) {
            System.out.println(player.getName() + " menang!");
        } else if (player.getHealth() <= 0 && enemy.getHealth() > 0) {
            System.out.println(enemy.getName() + " menang!");
        } else {
            System.out.println("Seri!");
        }
    }
}
